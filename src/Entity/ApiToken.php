<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApiTokenRepository")
 */
class ApiToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("token_main")
     * @ORM\Column(type="string", length=255)
     * @SerializedName("hash")
     */
    private $tokenHash;

    /**
     * @Groups("token_main")
     * @ORM\Column(type="datetime")
     * @SerializedName("expires at")
     */
    private $expiresAt;

    /**
     * @Groups("token_main")
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="apiTokens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->tokenHash = bin2hex(random_bytes(60));
        $this->expiresAt = new DateTime('+5 minutes');
        $user->addApiToken($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTokenHash(): ?string
    {
        return $this->tokenHash;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function isExpired(): bool
    {
        return $this->expiresAt <= new DateTime();
    }
}
