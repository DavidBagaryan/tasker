<?php

namespace App\Entity;

use App\Form\Model\UserUpdateModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    use TimestampableEntity;

    const BLANK_NAME_PLACEHOLDER = 'stranger in the night ;)';

    const TIME_REPRESENTATION_FORMAT = 'Y-m-d H:i:s';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"user_additional", "email_username"})
     * @ORM\Column(type="string", length=180, unique=true)
     * @SerializedName("email address")
     */
    private $email;

    /**
     * @Groups({"user_main", "email_username"})
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     */
    private $userName;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ApiToken", mappedBy="user", orphanRemoval=true)
     */
    private $apiTokens;

    /**
     * @ORM\OrderBy({"updatedAt" = "DESC"})
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="user", orphanRemoval=true)
     */
    private $tasks;

    public function __construct()
    {
        $this->apiTokens = new ArrayCollection();
        $this->tasks = new ArrayCollection();
    }

    public function populate(UserUpdateModel $userModel): void
    {
        $this->userName = $userModel->userName;
        $this->password = $userModel->password;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return empty($this->userName)
            ? self::BLANK_NAME_PLACEHOLDER
            : $this->userName;
    }

    public function setUserName(string $userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @Groups("user_main")
     * @SerializedName("date registration")
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt->format(self::TIME_REPRESENTATION_FORMAT);
    }

    /**
     * @Groups("user_main")
     * @SerializedName("last profile update")
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt->format(self::TIME_REPRESENTATION_FORMAT);
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|ApiToken[]
     */
    public function getApiTokens(): Collection
    {
        return $this->apiTokens;
    }

    public function addApiToken(ApiToken $apiToken): self
    {
        if (!$this->apiTokens->contains($apiToken)) {
            $this->apiTokens[] = $apiToken;
        }

        return $this;
    }

    /**
     * @Groups("user_public_tasks")
     * @return Collection|Task[]
     * @SerializedName("task list")
     */
    public function getPublicTasks(): Collection
    {
        return $this->tasks->filter(function (Task $task) {
            return !$task->isPersonal();
        });
    }

    /**
     * @Groups("user_all_tasks")
     * @return Collection|Task[]
     */
    public function getAllTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setUser($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getUser() === $this) {
                $task->setUser(null);
            }
        }

        return $this;
    }
}
