<?php

namespace App\Entity;

use App\Form\Model\TaskModel;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 * @ORM\Table(
 *     name="task",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="user_id_task_name", columns={"user_id", "name"})
 *     }
 * )
 * @UniqueEntity(fields={"user", "name"})
 */
class Task
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("task_main")
     * @ORM\Column(type="string", length=255)
     * @SerializedName("task name")
     */
    private $name;

    /**
     * @Groups("task_main")
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("task description")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tasks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCompleted = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPersonal = false;

    public function __construct(?TaskModel $taskModel = null)
    {
        if (is_null($taskModel)) {
            return;
        }

        $this->populate($taskModel);
    }

    public function populate(TaskModel $taskModel): void
    {
        $this->user = $taskModel->email;
        $this->name = $taskModel->name;
        $this->description = $taskModel->description;

        // little toggle =(
        $this->isPersonal = is_null($taskModel->isPersonal)
            ? false
            : $taskModel->isPersonal;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @Groups("task_main")
     * @SerializedName("created at")
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt->format(User::TIME_REPRESENTATION_FORMAT);
    }

    /**
     * @Groups("task_main")
     * @SerializedName("last update")
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt->format(User::TIME_REPRESENTATION_FORMAT);
    }

    public function isCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    public function setCompleted(): self
    {
        $this->isCompleted = true;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isPersonal(): ?bool
    {
        return $this->isPersonal;
    }

    public function setIsPersonal(bool $isPersonal): self
    {
        $this->isPersonal = $isPersonal;

        return $this;
    }

    /**
     * @Groups("task_details")
     * @return string
     * @SerializedName("is personal")
     */
    public function isPersonalRepresentation(): string
    {
        return $this->isPersonal
            ? 'yes'
            : "no, it's public";
    }
}
