<?php

namespace App\Validator;

use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserExistsValidator extends ConstraintValidator
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function validate($value, Constraint $constraint)
    {
        $existed = $this->userRepository
            ->findOneBy(['email' => $value]);

        if ($existed) {
            return;
        }

        /* @var $constraint UserExists */
        $this->context
            ->buildViolation($constraint->message)
            ->addViolation();
    }
}
