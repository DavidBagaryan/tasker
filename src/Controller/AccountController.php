<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Model\UserUpdateModel;
use App\Form\UserUpdateType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends AbstractController
{
    /**
     * @Rest\Get("/api/account-info/{id}", name="api_account_info")
     * @param User $user
     * @return Response
     */
    public function apiAccountInfo(User $user): Response
    {
        return $this->json(
            compact('user'),
            Response::HTTP_OK,
            [],
            ['groups' => ['user_main', 'user_public_tasks', 'task_main']]
        );
    }

    /**
     * @Rest\Put("/api/account/{id}/update", name="api_account_update")
     * @param User                         $user
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function editProfile(User $user, Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        if ($user !== $this->getUser()) {
            return $this->json('access denied', Response::HTTP_UNAUTHORIZED);
        }

        $userForm = $this->createForm(UserUpdateType::class);
        $userForm->submit(json_decode($request->getContent(), true));

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            /** @var UserUpdateModel $userModel */
            $userModel = $userForm->getData();
            $userModel->encodePassword($user, $encoder);
            $user->populate($userModel);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->json(
                compact('user'),
                Response::HTTP_ACCEPTED,
                [],
                ['groups' => ['user_main', 'user_additional']]
            );
        }

        return $this->json($userForm->getErrors()->getForm(), Response::HTTP_BAD_REQUEST);
    }
}
