<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class ApiTaskController extends AbstractFOSRestController
{
    /**
     * @Rest\Post("/tasks/personal-list", name="tasks_personal_list")
     * @return Response
     */
    public function getPersonalList(): Response
    {
        $user = $this->getUser();
        return $this->json(
            compact('user'),
            Response::HTTP_OK,
            [],
            ['groups' => ['user_main', 'user_additional', 'user_all_tasks', 'task_main']]
        );
    }

    /**
     * @Rest\Patch("/task/{id}/mark-complete", name="task_mark_complete")
     * @param Task $task
     * @return Response
     */
    public function completeTask(Task $task): Response
    {
        $this->denyAccessUnlessGranted("MANAGE", $task);

        if ($task->isCompleted()) {
            return $this->json(
                ['already completed', $task],
                Response::HTTP_FOUND,
                [],
                ['groups' => ['task_main', 'task_details']]
            );
        }

        $task->setCompleted();
        $em = $this->getDoctrine()->getManager();
        $em->persist($task);
        $em->flush();

        return $this->json($task, Response::HTTP_ACCEPTED, [], ['groups' => ['task_main', 'task_details']]);
    }

    /**
     * @Rest\Put("/task/{id}/edit", name="task_edit")
     * @param Task    $task
     * @param Request $request
     * @return Response
     */
    public function edit(Task $task, Request $request): Response
    {
        $this->denyAccessUnlessGranted("MANAGE", $task);

        $taskForm = $this->createForm(TaskType::class);
        $taskForm->submit(json_decode($request->getContent(), true));

        if ($taskForm->isSubmitted() && $taskForm->isValid()) {
            $taskDTO = $taskForm->getData();
            $task->populate($taskDTO);

            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            return $this->json(
                compact('task'),
                Response::HTTP_ACCEPTED,
                [],
                ['groups' => ['task_main', 'task_details']]
            );
        }

        return $this->json($taskForm->getErrors()->getForm(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Post("/tasks/add-task", name="add_task")
     * @param Request $request
     * @return Response
     */
    public function addTask(Request $request): Response
    {
        $taskForm = $this->createForm(TaskType::class);
        $taskForm->submit(json_decode($request->getContent(), true));

        if ($taskForm->isSubmitted() && $taskForm->isValid()) {
            $taskDTO = $taskForm->getData();
            $task = new Task($taskDTO);

            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            return $this->json(
                compact('task'),
                Response::HTTP_CREATED,
                [],
                ['groups' => ['task_main', 'task_details']]
            );
        }
        return $this->json($taskForm->getErrors()->getForm(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Delete("/task/{id}/delete", name="remove_task")
     * @param Task $task
     * @return Response
     */
    public function delete(Task $task): Response
    {
        $this->denyAccessUnlessGranted('MANAGE', $task);
        $taskName = $task->getName();
        $this->getUser()->removeTask($task);
        $em = $this->getDoctrine()->getManager();
        $em->remove($task);
        $em->flush();

        return $this->json(['deleted' => $taskName], Response::HTTP_ACCEPTED);
    }
}
