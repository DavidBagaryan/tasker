<?php

namespace App\Controller;

use App\Entity\ApiToken;
use App\Entity\User;
use App\Form\Model\UserRegisterModel;
use App\Form\UserRegistrationType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractFOSRestController
{
    /**
     * @Rest\Post("/api/register", name="api_register")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function apiRegister(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $regForm = $this->createForm(UserRegistrationType::class);
        $regForm->submit(json_decode($request->getContent(), true));

        if ($regForm->isSubmitted() && $regForm->isValid()) {
            /** @var UserRegisterModel $userDTO */
            $userDTO = $regForm->getData();
            $user = new User();
            $user->setEmail($userDTO->email);
            $user->setUserName($userDTO->userName);
            $user->setPassword($encoder->encodePassword(
                $user,
                $userDTO->plainPassword
            ));

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->json(
                compact('user'),
                Response::HTTP_CREATED,
                [],
                ['groups' => 'main']
            );
        }

        return $this->json($regForm->getErrors()->getForm());
    }

    /**
     * @Rest\Post("/api/generate-token", name="api_token_generate")
     * @return Response
     */
    public function apiTokenGenerate(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $lastToken = $user->getApiTokens()->last();

        if (!$lastToken || $lastToken->isExpired()) {
            $apiToken = new ApiToken($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($apiToken);
            $em->flush();
        } else {
            $apiToken = $lastToken;
        }

        return $this->json(
            compact('apiToken'),
            Response::HTTP_CREATED,
            [],
            ['groups' => ['token_main', 'email_username']]
        );
    }

    /**
     * @Rest\Post("/api/update-token", name="api_token_update")
     * @return Response
     */
    public function apiTokenUpdate(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $apiToken = new ApiToken($user);
        $em = $this->getDoctrine()->getManager();
        $em->persist($apiToken);
        $em->flush();

        return $this->json(
            compact('apiToken'),
            Response::HTTP_CREATED,
            [],
            ['groups' => ['token_main', 'email_username']]
        );
    }
}
