<?php

namespace App\Form\DataTransformer;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class EmailToUserTransformer implements DataTransformerInterface
{
    private $userRepository;

    private $finderCallback;

    public function __construct(UserRepository $userRepository, callable $finderCallback)
    {
        $this->userRepository = $userRepository;
        $this->finderCallback = $finderCallback;
    }

    public function transform($value): string
    {
        if (is_null($value)) {
            return '';
        }

        if (!$value instanceof User) {
            throw new TransformationFailedException('The UserSelectTextType can only be used with User objects');
        }

        return $value->getEmail();
    }

    public function reverseTransform($value): ?User
    {
        if (!$value) {
            return null;
        }

        $callback = $this->finderCallback;
        $user = $callback($this->userRepository, $value);

        if (is_null($user)) {
            throw new TransformationFailedException("User #{$value} not found");
        }

        return $user;
    }
}