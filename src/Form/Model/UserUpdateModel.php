<?php

namespace App\Form\Model;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserUpdateModel
{
    /**
     * @Assert\NotBlank(message="U should add a #username to show other users and guest your tasks")
     */
    public $userName;

    /**
     * @Assert\NotBlank(message="Choose a #password, hey you!")
     * @Assert\Length(
     *     min=5,
     *     minMessage="The #password needs to be more than 5 symbols",
     *     max=100,
     *     maxMessage="Please no longer than 100 symbols for #passford field"
     * )
     */
    public $plainPassword;

    public $password;

    public function encodePassword(User $user, UserPasswordEncoderInterface $encoder): void
    {
        $this->password = $encoder->encodePassword(
            $user,
            $this->plainPassword
        );
    }
}