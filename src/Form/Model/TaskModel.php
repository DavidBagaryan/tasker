<?php

namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class TaskModel
{
    /**
     * @Assert\NotBlank(message="property #name is requered")
     * @Assert\Length(min="5", minMessage="#task should be more complex")
     */
    public $name;

    /**
     * @Assert\NotBlank(message="Cannot create the task without a #user")
     */
    public $email;

    /**
     * @Assert\Length(
     *     min="15",
     *     minMessage="Let's make the #description more open",
     *     max="400",
     *     maxMessage="It's too long for the task #description property. Don't rush and let's try to make it less long",
     * )
     */
    public $description;

    /**
     * @var bool
     */
    public $isPersonal = false;
}