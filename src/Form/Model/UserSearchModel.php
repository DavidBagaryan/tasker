<?php

namespace App\Form\Model;

use App\Validator\UserExists;
use Symfony\Component\Validator\Constraints as Assert;

class UserSearchModel
{
    /**
     * @Assert\NotBlank(message="Email cannot be blank")
     * @Assert\Email(message="Email shold be a valid email address")
     * @UserExists()
     */
    public $email;

    /**
     * @Assert\NotBlank(message="Choose a password, hey you!")
     * @Assert\Length(min=5, minMessage="The password is too short (more or equals 5 symbols is required)")
     */
    public $plainPassword;
}