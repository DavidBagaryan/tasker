<?php

namespace App\Form\Model;

use App\Validator\UniqueUser;
use Symfony\Component\Validator\Constraints as Assert;

class UserRegisterModel
{
    /**
     * @Assert\NotBlank(message="Property #email cannot be blank")
     * @Assert\Email(message="Property #email shold be a valid email address")
     * @UniqueUser()
     */
    public $email;

    /**
     * @Assert\NotBlank(message="U should add a #username to show other users and guest your tasks")
     */
    public $userName;

    /**
     * @Assert\NotBlank(message="Choose a #password, hey you!")
     * @Assert\Length(
     *     min=5,
     *     minMessage="The #password needs to be more than 5 symbols",
     *     max=100,
     *     maxMessage="Please no longer than 100 symbols for #passford field"
     * )
     */
    public $plainPassword;


}