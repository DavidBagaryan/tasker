<?php

namespace App\Form;

use App\Entity\Task;
use App\Entity\User;
use App\Form\Model\TaskModel;
use App\Repository\TaskRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{
    private $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('email', UserSelectEmailType::class)
            ->add('description')
            ->add('isPersonal')
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) {
                    if (!$event->getData()) {
                        return;
                    }
                    return $this->checkTaskForUniqueness($event);
                });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => TaskModel::class,
            'csrf_protection' => false,
        ]);
    }

    private function checkTaskForUniqueness(FormEvent $event): void
    {
        $formData = $event->getData();

        /** @var User $user */
        $user = $formData->email;
        $taskName = $formData->name;
        if (is_null($user) || is_null($taskName)) {
            return;
        }

        $matches = $user->getAllTasks()->filter(function (Task $task) use ($taskName) {
            return $task->getName() === $taskName;
        })->count();

        if ($matches) {
            $event->getForm()->addError(new FormError('The task with the same name already exists'));
        }
        return;
    }
}
