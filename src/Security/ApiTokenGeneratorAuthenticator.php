<?php

namespace App\Security;

use App\Entity\User;
use App\Form\UserSearchFormType;
use App\Repository\UserRepository;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Serializer\SerializerInterface;

class ApiTokenGeneratorAuthenticator extends AbstractGuardAuthenticator
{
    private $userSearchForm;

    private $encoder;

    private $userRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(
        FormFactoryInterface $factory,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $encoder,
        SerializerInterface $serializer
    )
    {
        $this->userSearchForm = $factory->create(UserSearchFormType::class);
        $this->userRepository = $userRepository;
        $this->encoder = $encoder;
        $this->serializer = $serializer;
    }

    public function supports(Request $request): bool
    {
        return $request->attributes->get('_route') === 'api_token_generate'
            && $request->isMethod(Request::METHOD_POST);
    }

    public function getCredentials(Request $request)
    {
        $searchForm = $this->userSearchForm;
        $searchForm->submit(json_decode($request->getContent(), true));

        $errors = $searchForm->getErrors();
        if ($errors->count() !== 0) {
            throw new CustomUserMessageAuthenticationException($errors->current()->getMessage());
        }

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            return $searchForm->getData();
        }

        $message = $this->serializer->serialize(
            $searchForm->getErrors()
                       ->getForm(),
            'json'
        );
        throw new CustomUserMessageAuthenticationException($message);
    }

    public function getUser($credentials, UserProviderInterface $userProvider): ?User
    {
        $user = $this->userRepository->findOneBy(['email' => $credentials->email]);

        if (!$user) {
            throw new CustomUserMessageAuthenticationException('User not found!');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        if ($this->encoder->isPasswordValid($user, $credentials->plainPassword)) {
            return true;
        }
        throw new CustomUserMessageAuthenticationException('Password is invalid!');
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        return null;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse('authentication required', Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
