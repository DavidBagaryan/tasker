<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\ApiTokenRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ApiTokenAuthenticator extends AbstractGuardAuthenticator
{
    private $tokenRepository;

    private $guardedMethods = [
        Request::METHOD_PUT,
        Request::METHOD_POST,
        Request::METHOD_PATCH,
        Request::METHOD_DELETE,
    ];

    public function __construct(ApiTokenRepository $tokenRepository)
    {
        $this->tokenRepository = $tokenRepository;
    }

    public function supports(Request $request): bool
    {
        return $request->attributes->get('_route') !== 'api_token_generate'
            && in_array($request->getMethod(), $this->guardedMethods);
    }

    public function getCredentials(Request $request): array
    {
        return [
            'userEmail' => $request->get('email'),
            'token'     => substr($request->headers->get('Authorization'), 7) ?? ''
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider): ?User
    {
        if (empty($credentials)) {
            throw new CustomUserMessageAuthenticationException('An empty token has passed');
        }

        $token = $this->tokenRepository->findOneBy(['tokenHash' => $credentials['token']]);

        if (!$token) {
            throw new CustomUserMessageAuthenticationException('User not found!');
        }

        if ($token->isExpired()) {
            throw new CustomUserMessageAuthenticationException('Token expired');
        }

        if ($token->getUser()->getEmail() !== $credentials['userEmail']) {
            throw new CustomUserMessageAuthenticationException('Access denied');
        }

        return $token->getUser();
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        return null;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse('authentication required', Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
